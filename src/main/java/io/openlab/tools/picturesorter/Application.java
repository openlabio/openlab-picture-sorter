package io.openlab.tools.picturesorter;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Organize a source tree of JPG files into a target tree.
 *
 * @author Vincent DE BAERE
 */
public class Application {

    private final ApplicationContext context;

    /**
     * Launches the picture sorting application.
     *
     * @param argv must contain exactly two directories: source and target
     */
    public static void main(String[] argv) {

        if (argv.length != 2) {
            System.err.println("Incorrect number of arguments.");
            System.exit(1);
        }
        try {
            Application app = new Application(new File(argv[0]),
                    new File(argv[1]));
            app.run();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Constructs a new Application instance.
     *
     * @param sourceDirectory
     * @param targetDirectory
     */
    public Application(File sourceDirectory, File targetDirectory) {
        context = new ApplicationContext(sourceDirectory, targetDirectory);
        context.setFilter((File pathname) -> (pathname.isDirectory()
                || pathname.getName().endsWith("jpg")
                || pathname.getName().endsWith("JPG")));
    }

    public void run() {
        /* Set up the tree iterator thread. */
        Thread treeIteratorThread = new Thread(() -> {
            TreeIterator iterator = new TreeIterator(context);
            iterator.iterate();
        });
        treeIteratorThread.setName("TreeIteratorThread");



        /* Start the tree iterator. */
        treeIteratorThread.start();

        /* When the tree iterator is done, await execution of all tasks. */
        try {
            treeIteratorThread.join();
            context.getExecutor().shutdown();
            while(!context.getExecutor().awaitTermination(1, TimeUnit.MINUTES)) {
                ;
            }
        } catch (InterruptedException iex) {
            System.exit(1);
        }

    }
}
