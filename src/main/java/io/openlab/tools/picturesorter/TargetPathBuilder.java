package io.openlab.tools.picturesorter;

import java.io.File;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * @author Vincent DE BAERE
 */
public class TargetPathBuilder {

    private static final String DEFAULT_UNKNOWN_DATE_DIR = "unclassified";

    private static final DateTimeFormatter Y_FMT
            = DateTimeFormatter.ofPattern("yyyy")
                    .withZone(ZoneId.systemDefault());
    private static final DateTimeFormatter M_FMT
            = DateTimeFormatter.ofPattern("MM")
                    .withZone(ZoneId.systemDefault());
    private static final DateTimeFormatter F_FMT
            = DateTimeFormatter.ofPattern("yyyyMMddHHmm")
                    .withZone(ZoneId.systemDefault());

    public static File buildTargetPath(ImageContext imageContext) {
        File dirname = imageContext.getApplicationContext()
                .getTargetDirectory();

        if (imageContext.getOriginalCreationInstant() == null) {
            dirname = new File(dirname, DEFAULT_UNKNOWN_DATE_DIR);
            return new File(dirname, imageContext.getDigest() + ".jpg");
        } else {
            dirname = new File(dirname,
                    Y_FMT.format(imageContext.getOriginalCreationInstant()));
            dirname = new File(dirname,
                    M_FMT.format(imageContext.getOriginalCreationInstant()));
            return new File(dirname,
                    F_FMT.format(imageContext.getOriginalCreationInstant())
                    + "_" + imageContext.getDigest().substring(0, 8)
                    + ".jpg");
        }

    }
}
