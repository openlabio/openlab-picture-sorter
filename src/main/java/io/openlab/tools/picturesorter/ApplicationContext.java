package io.openlab.tools.picturesorter;

import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Vincent DE BAERE
 */
public class ApplicationContext {

    private final File sourceDirectory;
    private final File targetDirectory;
    private final ThreadPoolExecutor executor;

    private FileFilter filter;

    public ApplicationContext(File sourceDirectory, File targetDirectory) {
        if (sourceDirectory == null || targetDirectory == null) {
            throw new NullPointerException();
        }

        if (!sourceDirectory.isDirectory() || !sourceDirectory.canRead()) {
            throw new IllegalArgumentException("Invalid source directory");
        }
        this.sourceDirectory = sourceDirectory;

        if (!targetDirectory.exists() && !targetDirectory.mkdirs()) {
            throw new IllegalArgumentException("Invalid target directory");
        }
        this.targetDirectory = targetDirectory;

        /* Set up the processing thread pool. */
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        executor = new ThreadPoolExecutor(availableProcessors,
                availableProcessors, 10, TimeUnit.MINUTES,
                new LinkedBlockingQueue<>());
    }

    public final File getSourceDirectory() {
        return sourceDirectory;
    }

    public final File getTargetDirectory() {
        return targetDirectory;
    }

    public FileFilter getFilter() {
        return filter;
    }

    public void setFilter(FileFilter filter) {
        this.filter = filter;
    }

    public ThreadPoolExecutor getExecutor() {
        return executor;
    }
}
