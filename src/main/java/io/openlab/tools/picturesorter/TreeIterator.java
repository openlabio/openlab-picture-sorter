package io.openlab.tools.picturesorter;

import java.io.File;

/**
 *
 * @author Vincent DE BAERE
 */
public class TreeIterator {

    private final ApplicationContext context;

    public TreeIterator(ApplicationContext context) {
        this.context = context;
    }

    public void iterate() {
        for (File f : context.getSourceDirectory()
                .listFiles(context.getFilter())) {
            iterate(f);
        }
    }

    private void iterate(File path) {
        if (path.equals(context.getTargetDirectory())) {
            return;
        }
        if (path.isDirectory()) {
            for (File f : path.listFiles(context.getFilter())) {
                iterate(f);
            }
        } else {
            context.getExecutor().execute(
                    new FileHandler(
                            new ImageContext(path, context)
                    )
            );
        }
    }
}
