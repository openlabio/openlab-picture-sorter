package io.openlab.tools.picturesorter;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;

/**
 * @author Vincent DE BAERE
 */
public class MetadataExtractor {

    private static final Logger logger =
            Logger.getLogger(MetadataExtractor.class.getCanonicalName());

    public static Instant extractOriginalCreationInstant(File file)
            throws IOException {
        try {
            Metadata md = ImageMetadataReader.readMetadata(file);
            ExifSubIFDDirectory sid
                    = md.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
            if (sid == null) {
                return null;
            }

            Date d = sid.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
            if(d == null) {
                return null;
            }

            return d.toInstant();
        } catch (ImageProcessingException ipx) {
            logger.log(Level.WARNING, "Image processing went wrong.", ipx);
            return null;
        }
    }
}
