package io.openlab.tools.picturesorter;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Vincent DE BAERE
 */
public class FileHandler implements Runnable {

    private static final Logger logger
            = Logger.getLogger(FileHandler.class.getCanonicalName());

    private final ImageContext imageContext;

    public FileHandler(ImageContext imageContext) {
        this.imageContext = imageContext;
    }

    @Override
    public void run() {
        try {
            imageContext.setDigest(
                    FileDigester.digest(imageContext.getSourceFile()));
            imageContext.setOriginalCreationInstant(
                    MetadataExtractor.extractOriginalCreationInstant(
                            imageContext.getSourceFile()));

            File targetFile = TargetPathBuilder.buildTargetPath(imageContext);
            targetFile.getParentFile().mkdirs();

            Files.copy(imageContext.getSourceFile().toPath(),
                    targetFile.toPath(), StandardCopyOption.COPY_ATTRIBUTES);

            logger.log(Level.WARNING, "{0}\t{1}", new Object[]{
                imageContext.getSourceFile().getAbsolutePath(),
                targetFile.getAbsolutePath()
            });
        } catch (FileAlreadyExistsException fax) {
            logger.log(Level.INFO,
                    "Discarding {0} as duplicate.",
                    imageContext.getSourceFile().getAbsolutePath());
        } catch (IOException iox) {
            logger.log(Level.WARNING, "IO Exception.", iox);
        }

    }

}
