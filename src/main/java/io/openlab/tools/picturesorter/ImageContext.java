package io.openlab.tools.picturesorter;

import java.io.File;
import java.time.Instant;

/**
 *
 * @author Vincent DE BAERE
 */
public class ImageContext {

    private final ApplicationContext applicationContext;
    private final File sourceFile;

    private String digest;
    private Instant originalCreationInstant;

    public ImageContext(File sourceFile,
            ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        this.sourceFile = sourceFile;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public File getSourceFile() {
        return sourceFile;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public Instant getOriginalCreationInstant() {
        return originalCreationInstant;
    }

    public void setOriginalCreationInstant(Instant originalCreationInstant) {
        this.originalCreationInstant = originalCreationInstant;
    }

}
