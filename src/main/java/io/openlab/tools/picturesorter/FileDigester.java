package io.openlab.tools.picturesorter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Vincent DE BAERE
 */
public class FileDigester {

    private static final Logger logger
            = Logger.getLogger(FileDigester.class.getCanonicalName());

    private static final String HASH_ALGORITHM = "MD5";
    private static final int CHUNK_SIZE = 8196;

    public static String digest(File f) throws IOException {
        try (FileInputStream fis = new FileInputStream(f);
                Formatter formatter = new Formatter()) {

            MessageDigest digester
                    = MessageDigest.getInstance(HASH_ALGORITHM);
            byte[] chunk = new byte[CHUNK_SIZE];
            int bytesRead = 0;

            while ((bytesRead = fis.read(chunk)) != -1) {
                digester.update(chunk, 0, bytesRead);
            }

            byte[] digest = digester.digest();

            for (byte b : digest) {
                formatter.format("%02x", b);
            }

            return formatter.toString();

        } catch (NoSuchAlgorithmException ex) {
            logger.log(Level.SEVERE, "Digest algorithm not found.", ex);
            throw new RuntimeException(ex);
        }
    }

}
